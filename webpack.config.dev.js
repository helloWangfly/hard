/*
 * @Autor: wangchao
 * @Date: 2019-06-26 17:26:38
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-15 17:16:52
 * @Description: file content
 */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackBar = require('webpackbar');


const rules = {
    rules: [
        // {
        //     test:/\.js$/,
        //     exclude: /node_modules/,
        //     use:{
        //         loader:'eslint-loader',
        //         options:{
        //             enforce:'pre'
        //         }
        //     }
        // },
        {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', { 'useBuiltIns': 'usage' }],//{'debug':true}
                        ["@babel/preset-react"],
                    ],
                    plugins: [
                        ['@babel/plugin-proposal-decorators', { "legacy": true }],
                        [
                            "@babel/plugin-transform-runtime",
                            {
                                "absoluteRuntime": false,
                                "corejs": 2,
                                "helpers": true,
                                "regenerator": true,
                                "useESModules": false
                            }
                        ],
                        ["@babel/plugin-proposal-class-properties", { "loose": true }],
                        ["import", { "libraryName": "antd-mobile", "style": 'css' }, 'antd-mobile'],
                        ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": 'css' }, 'antd'] // `style: true` 会加载 less 文件
                    ]
                }
            }
        },
        {
            test: /\.css$/i,
            use: [
                "style-loader",
                'css-loader'
            ],
        },
        {
            test: /\.(scss)$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader?modules", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        },
        {
            test: /\.less$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                'css-loader', // translates CSS into CommonJS
                'less-loader', // compiles Less to CSS
            ]
        },
        {
            test: /\.(png|jpe?g|gif|svg)$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'images/'
                    },
                },
                {
                    loader: 'image-webpack-loader',
                }
            ],
        },
    ]
}

const entry = {
    entry: {
        app: './src/App.js',
        hello: './src/hello.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'hard',
            filename: 'index.html',
            template: './src/public/index.html',
            hash: true,
            favicon: './src/logo.ico',
            chunks: ['app']
        }),
        new HtmlWebpackPlugin({
            title: 'hello',
            filename: 'hello.html',
            template: './src/public/hello.html',
            hash: true,
            favicon: './src/logo.ico',
            chunks: ['hello']
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'css/[name].css',
            chunkFilename: 'css/[id].css',
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new WebpackBar(),
        new webpack.DefinePlugin({
            BUILD_ENV: JSON.stringify('development')
        })
    ]
}

const output = {
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].[hash].bundle.js",
    },
    module: rules,
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 8082,
        hot: true,
        // overlay: true
    },
    mode: 'development',
    resolve: {
        extensions: ['.js', '.jxs', '.json'], // 表示，这几个文件的后缀名，可以省略不写
        alias: { //表示别名
            '@': path.join(__dirname, './src') // 这样，@ 就表示项目根目录中 src 的这一层路径
        }
    },
    // devtool:'eval-source-map'
}

Object.assign(entry, output)

module.exports = entry;

// module.exports = (env,argv)=>{
//     entry.plugins = entry.plugins.concat([
//         new webpack.DefinePlugin({
//             BUILD_ENV : JSON.stringify(argv.mode)
//         })
//     ])
//     return entry
// }