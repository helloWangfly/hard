/*
 * @Autor: wangchao
 * @Date: 2019-07-23 17:07:34
 * @LastEditors: wangchao
 * @LastEditTime: 2019-07-23 17:07:34
 * @Description: file content
 */
export const routerMaps = [
  {
    path: "/Root",
    component: "/Root"
  },
  {
    path: "/About",
    component: "/About"
  },
  {
    path: "/Vip",
    component: "/Vip"
  },
  {
    path: "/VList",
    component: "/VList"
  },
  {
    path: '/Detail',
    component: '/Detail'
  },
  {
    path: '/Hook',
    component: '/Hook'
  },
  {
    path: '/DefineHook',
    component: '/DefineHook'
  },
  {
    path: "/Aa",
    component: "/Aa"
  },
  {
    path: "/Bb",
    component: "/Bb"
  },
  {
    path: "/Cc",
    component: "/Cc"
  },
  {
    path: "/Test",
    component: "/Test"
  },
  {
    path: "/Viewer",
    component: "/Viewer"
  },
  {
    component: "/NoMatch"
  },
]