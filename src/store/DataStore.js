/*
 * @Autor: wangchao
 * @Date: 2019-08-12 15:33:03
 * @LastEditors: wangchao
 * @LastEditTime: 2019-08-25 23:43:59
 * @Description: 加载基于redux的store
 */

'use strict'

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Index from './reducers';
import logger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';

const createStoreWithMiddleware = BUILD_ENV === 'development' ? composeWithDevTools(applyMiddleware(thunk, logger))(createStore) : applyMiddleware(thunk)(createStore);

const store = createStoreWithMiddleware(Index);

export default store;