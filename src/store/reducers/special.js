/*
 * @Autor: wangchao
 * @Date: 2019-08-13 13:24:08
 * @LastEditors: wangchao
 * @LastEditTime: 2019-08-25 18:11:15
 * @Description: file content
 */
import { ActionTypes } from '../constants';
const initState = {
    type: 'SPECIAL',
    data: [],
}


const special = (state = initState, action) => {
    switch (action.type) {
        case ActionTypes.SPECIAL:
            return Object.assign({}, state, {
                type: action.type,
                data: action.data
            })
        case '0327':
            return Object.assign({}, state, {
                type: '0327',
                data: '数据'
            })
        default:
            return state
    }

}

export default special;