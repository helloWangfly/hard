import {combineReducers } from 'redux';
import common from './common';
import special from './special';

const rootReducer = combineReducers({
    common,
    special
})

export default rootReducer;