import React, { Component, useState,useEffect } from 'react';
import {Button,Spin} from 'antd'
import RViewerJS from 'viewerjs-react'
import 'viewerjs-react/dist/index.css'

const Viewer=()=>{

  const [selectedPerson,setSelectedPerson]=useState('');
  const [data, setData]=useState();
  const [loading, setLoading]=useState(false)

  const arr=[
    {
      title:'小王',
      images:[{
        title:'图片',
        url:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1113%2F061H0102U6%2F20061G02U6-12-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1644304629&t=35c3de4986f08ca3914750b8ba164019'
      },{
        title:'图片',
        url:'https://pic.rmb.bdstatic.com/1530971282b420d77bdfb6444d854f952fe31f0d1e.jpeg'
      },{
        title:'图片',
        url:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2Ftp01%2F1ZZQ20QJS6-0-lp.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1644304629&t=53e1d129403046ec53898d8b155c264b'
      }]
  },{
    title:'小李',
    images:[{
      title:'图片',
      url:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Ffile03.16sucai.com%2F2017%2F1100%2F16sucai_p571c007029.JPG&refer=http%3A%2F%2Ffile03.16sucai.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1644304629&t=feef5986c646526a875999d3e1302934'
    },{
      title:'图片',
      url:'https://pic.rmb.bdstatic.com/1530971282b420d77bdfb6444d854f952fe31f0d1e.jpeg'
    },{
      title:'图片',
      url:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F4k%2Fs%2F02%2F2109242332225H9-0-lp.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1644304629&t=b41a11fab8eed9ebb0f81aeacd1f49cf'
    }]
}
]

  useEffect(() => {
    setData(arr)
  }, []);

  useEffect(() => {
    if(arr.length){
      if(!selectedPerson)setSelectedPerson(arr[0].title)  
    }
  }, [arr]);

  const fresh=()=>{
    setLoading(true);
    setTimeout(() => {
      setData(arr);
      setLoading(false)
    },2000);
  }

  
  return (
    <Spin spinning={loading}>
    <RViewerJS>
      <div>
      {arr.map(it=>(<Button onClick={()=>setSelectedPerson(it.title)} key={it.title} type={selectedPerson===it.title?"primary":''} style={{margin:'10px'}}>{it.title}</Button>))}
      </div>
      <div className="images" style={{display:'flex'}}>
        {selectedPerson?arr.find(it=>it.title===selectedPerson).images.map((item,index)=><img style={{width:'100px',height:'100px'}} alt="ll" src={item.url}  key={index} />):''}
        </div>
        <Button onClick={fresh}>刷新</Button>
    </RViewerJS>
    </Spin>
    )
}

export default Viewer