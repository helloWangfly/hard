import React, { Component } from 'react';
import { List, message, Spin } from 'antd';
import { List as VList, WindowScroller } from 'react-virtualized';
import 'react-virtualized/styles.css';

import '@/style/VList.less';

let data = []
for (let i = 0; i < 10000; i++) {
  data.push({
    name: `name${i}`,
    id: i
  })
}

class VirtualizedList extends Component {

  state = {
    list: [],
    loading: true,
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        list: data,
        loading: false
      })
    }, 500)
  }


  rowRenderer = ({
    key,            // 唯一值
    index,          // 索引号
    isScrolling,    // 是否在滚动中
    isVisible,      // 当前行在list中是否可见
    style,
  }) => {
    const { list } = this.state
    const data = list[index]
    const cssObj = {
      lineHeight: '30px',
      borderBottom: 'solid 1px #ddd',
      padding: '0 10px'
    }
    return (
      <div key={key} style={{ ...style, ...cssObj }}>
        {data.name}
      </div>
    );
  }



  render() {
    const { list, loading } = this.state
    const height = parseInt(document.body.clientHeight);
    const rowHeight = 30;
    const width = document.querySelector('.container') && parseInt(document.querySelector('.container').clientWidth);

    return (
      <div className='container'>
        <div className='v_bold'>虚拟列表：</div>
        {
          loading ? 'loading...' :
            <VList
              width={width}
              height={300}
              rowCount={list.length}
              rowHeight={rowHeight}
              rowRenderer={this.rowRenderer}
            />
        }
      </div>
    )
  }
}

export default VirtualizedList
