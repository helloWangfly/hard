/*
 * @Autor: wangchao
 * @Date: 2019-09-19 18:20:34
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-19 18:51:33
 * @Description: file content
 */

import React from 'react';
import FormatUtil from '../../utils/FormatUtil';
import { HigherComponent } from '../../base/HigherComponent';
import { LinkTo } from '../../utils/LinkTo'

@HigherComponent()
export default
    class Detail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {}
        }

    }

    componentWillMount() {

    }

    componentDidMount() {
        FormatUtil.setTitle('detail页面')

        const query = LinkTo.getParams(this.props.location.search)
        this.setState({
            data: query.data
        })
    }

    render() {
        const { data } = this.state
        return (
            <div>
                <div>{data.name}</div>
            </div>
        )
    }


}