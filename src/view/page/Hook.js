import React, { useState, useEffect, useReducer, memo, useMemo, useCallback, useRef } from 'react';
import TabBars2 from '../component/TabBars2';
import Container from '../commonComponent/Container';

const Hook = (props) => {
    // console.log('22222222222', count)

    const [count, setCount] = useState({ count: 0 })
    // console.log('1111111111111', count, number)


    // const [count, dispath] = useReducer((state, action) => {
    //     console.log('5555555555555', state, action)
    //     if (action.type == 'add') {
    //         return {count:state.count }
    //     }
    // }, {count:8});

    // useEffect(() => {
    //     setTimeout(() => {
    //         console.log('sssssssssstttttttt')
    //     }, 3000);
    //     return () => {
    //         console.log('444444444444')
    //     }
    // })

    useEffect(() => {
        console.log('999999999999', count)
        return () => {
            console.log('组件卸载。。。', props);
        }
    },[])


    // const inputEl = useRef();
    // const onButtonClick = () => {
    //   // `current` 指向已挂载到 DOM 上的文本输入元素
    //   inputEl.current.focus();
    //   console.log('oooooooooooo', inputEl,inputEl.current)
    // };
    // console.log('6666666666666', inputEl,inputEl.current)


    // const umemo = useMemo(()=>{
    //     console.log('usememo');
    //     return <button>{'usememo---count值为：'+ count.count}</button>;
    // },[count])

    // const ucallback = useCallback(()=>{
    //     console.log('cccccback',count.count);
    //     return `callback---count值为：${count.count}`
    // },[count])

    // const Memo = memo(({ data, test, count }) => {
    //     console.log('mmmmmmmmmmm123', count)
    //     return <div>{`mmmmm${data}-${test}`}</div>
    // })


    return (
        <Container>
            <div style={{overflow:'auto',flex:1}}>
                <p> you clicked {count.count}</p>
                {/* <p>{umemo}</p> */}
                {/* <p>{ucallback()}</p> */}
                {/* <Memo data='data' test='test' count={count} /> */}

                {/* <button onClick={() => {dispath({type:'add',name:'wangchao'})  }}>click me</button> */}
                <button onClick={() => { setCount((state) => ({ count: state.count + 1 })) }}>click me</button>

                {/* <input ref={inputEl} type="text" />
                    <button onClick={onButtonClick}>Focus the input</button> */
                }
            </div>

            <TabBars2 selectedTab='/Hook'></TabBars2>
        </Container>
    )
}

export default Hook;