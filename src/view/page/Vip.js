/*
 * @Autor: wangchao
 * @Date: 2019-07-23 14:47:34
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-20 15:28:56
 * @Description: file content
 */
import React from 'react';
import FormatUtil from '../../utils/FormatUtil';
import { HigherComponent } from '../../base/HigherComponent';
import { LinkTo } from '../../utils/LinkTo';
import ListView from '../commonComponent/ListView';
import EmptyView from '../commonComponent/EmptyView';
import APIService, { YH_HttpResponseStatus } from '../../service/APIService';
import TabBars2 from '../component/TabBars2';
import Container from '../commonComponent/Container';

@HigherComponent()
export default
    class Vip extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            dataList: []
        }

    }

    componentDidMount() {
        FormatUtil.setTitle('vip页面')

        this.fetchUsers()
    }

    componentDidUpdate() {
        //回到上次浏览位置
        const scrollTop = FormatUtil.getSessionStorage('scrollTop')
        // window.scrollTo(0, scrollTop)
        $('.listView').scrollTop(scrollTop)
        sessionStorage.removeItem('scrollTop')
    }

    fetchUsers = () => {
        fetch('http://5d832583c9e34100140710db.mockapi.io/users/data').then(response => {
            return response.json()
        }).then(json => {
            this.setState({
                isLoading: false,
                dataList: json
            })
        })
    }

    goDetail = (item) => {
        LinkTo.push({
            pathname: '/Detail',
            query: {
                data: item
            }
        })
    }


    renderItem = (item, index) => {
        return (
            <div onClick={() => { this.goDetail(item) }} key={index} style={{ display: 'flex', borderBottom: 'solid 1px #ddd', marginTop: '10px' }}>
                <div style={{ width: '120px', height: '90px', backgroundColor: "#f4f4f4", flex: 'none' }}></div>
                <div style={{ flex: 'auto', paddingLeft: '10px' }}>
                    <div>{item.name}</div>
                    <div>{item.desc}</div>
                </div>
            </div>
        )
    }

    renderEmpty = () => {
        return <EmptyView imgUrl={require('../../images/super.png')} title={'哇哦，暂无信息~'} />
    }

    getScrollTop = () => {
        // const scrollTop = document.documentElement.scrollTop
        const scrollTop = $('.listView').scrollTop()
        return scrollTop
    }

    componentWillUnmount() {
        FormatUtil.setSessionStorage('scrollTop', this.getScrollTop())
    }

    render() {
        const { isLoading, dataList } = this.state
        return (
            <Container>
                <ListView
                    isLoading={isLoading}
                    dataSource={dataList}
                    renderItem={this.renderItem}
                    renderEmpty={this.renderEmpty}
                />
                <TabBars2 selectedTab='/Vip'></TabBars2>
            </Container>
        )
    }


}