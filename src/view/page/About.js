/*
 * @Autor: wangchao
 * @Date: 2019-07-23 14:47:24
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-20 15:23:44
 * @Description: file content
 */
import React from 'react';
import FormatUtil from '../../utils/FormatUtil';
import { HigherComponent } from '../../base/HigherComponent';
import { LinkTo } from '../../utils/LinkTo'
import TabBars2 from '../component/TabBars2';
import Container from '../commonComponent/Container';

@HigherComponent()
export default
    class About extends React.Component {
    constructor(props) {
        super(props)


    }

    componentDidMount() {
        FormatUtil.setTitle('about页面')

    }

    goToPage = () => {
        LinkTo.push({
            pathname: '/vip',
            query: {
                param: '123'
            }
        })
    }

    render() {
        return (
            <Container>
                <div style={{overflow:'auto',flex:1}}>
                    <div>About~~~</div>
                </div>
                {/* <div onClick={this.goToPage}>LinkTo ---> vip</div> */}
                <TabBars2 selectedTab='/About'></TabBars2>
            </Container>
        )
    }


}