import React, { useState, useEffect } from 'react'
import useInterval from '../component/DefineHook'
export default props => {
    const [count, setCount] = useState(5);
    console.log('1111111111');
    useInterval(() => {
        console.log('555555555555',count);
        if(count > 0){
            setCount(count - 1);
        }
    }, count ? 1000 : null);
    console.log('3333333333');
    useEffect(()=>{
        console.log('444444444');
    })
    return (
        <div>
            {count < 1 ? (
                <div onClick={() => setCount(5)}>重新获取</div>
            ) : (
                    <div>{count}s后重新发送</div>
                )
            }
        </div>
    );
};