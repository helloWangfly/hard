import React, { Component } from 'react';
// import Child from './Child';


// export default class Test extends Component {
//     constructor(props) {
//         super(props)

//         this.state = {
//             isShow: true
//         }
//     }

//     show = () => {
//         const { isShow } = this.state
//         this.setState({
//             isShow: !isShow
//         })
//     }

//     renderState = () => {
//         this.setState({
//             test: 'ttt'
//         })
//     }

//     componentWillUpdate(nextProps, nextState) {

//         console.log('componentWillUpdate-parent', '-', nextProps, '-', nextState, '-', this.state);
//     }

//     componentDidUpdate(prevProps, prevState) {

//         console.log('componentDidUpdate-parent', '-', prevProps, '-', prevState);
//     }

//     render() {
//         const { isShow } = this.state
//         return (
//             <div>
//                 {
//                     isShow ? <Child /> : 'child消失了'
//                 }
//                 <hr />
//                 <button onClick={() => { this.show() }}>{isShow ? '隐藏child' : '展示child'}</button>
//                 <button onClick={() => { this.renderState() }}>渲染state</button>
//             </div>
//         )
//     }
// } 


import { Table, Input, InputNumber, Popconfirm, Form } from 'antd';
import Select22 from 'react-select-virtualized';
import '@/style/index.less';

const data = [];
for (let i = 0; i < 100; i++) {
    data.push({
        key: i.toString(),
        name: `Edrward ${i}`,
        age: 32,
        address: `London Park no. ${i}`,
    });
}
const EditableContext = React.createContext();

let list = []
for (let i = 0; i < 50000; i++) {
    list.push({
        label: `name${i}`,
        value: i,
        name: `cc${i}`,
        age: `age${i}`,
    })
}
class EditableCell extends React.Component {
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <Select22 options={list} />;
        }
        return <Input />;
    };

    renderCell = ({ getFieldDecorator }) => {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps}>
                {editing ? (
                    <Form.Item style={{ margin: 0 }} className='cc'>
                        {getFieldDecorator(dataIndex, {
                            rules: [
                                {
                                    required: true,
                                    message: `Please Input ${title}!`,
                                },
                            ],
                            initialValue: record[dataIndex],
                        })(this.getInput())}
                    </Form.Item>
                ) : (
                    children
                )}
            </td>
        );
    };

    render() {
        return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
    }
}

@Form.create()
class EditableTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data, editingKey: '' };
        this.columns = [
            {
                title: 'name',
                dataIndex: 'name',
                width: '25%',
                editable: true,
            },
            {
                title: 'age',
                dataIndex: 'age',
                width: '30%',
                editable: true,
            },
            {
                title: 'address',
                dataIndex: 'address',
                width: '30%',
                editable: true,
            },
            {
                title: 'operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    const { editingKey } = this.state;
                    const editable = this.isEditing(record);
                    return editable ? (
                        <span>
                            <EditableContext.Consumer>
                                {form => (
                                    <a
                                        onClick={() => this.save(form, record.key)}
                                        style={{ marginRight: 8 }}
                                    >
                                        Save
                                    </a>
                                )}
                            </EditableContext.Consumer>
                            <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                <a>Cancel</a>
                            </Popconfirm>
                        </span>
                    ) : (
                        <a disabled={editingKey !== ''} onClick={() => this.edit(record.key)}>
                            Edit
                        </a>
                    );
                },
            },
        ];
    }

    isEditing = record => record.key === this.state.editingKey;

    cancel = () => {
        this.setState({ editingKey: '' });
    };

    save(form, key) {
        form.validateFields((error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => key === item.key);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
                this.setState({ data: newData, editingKey: '' });
            } else {
                newData.push(row);
                this.setState({ data: newData, editingKey: '' });
            }
        });
    }

    edit(key) {
        this.setState({ editingKey: key });
    }

    render() {
        const components = {
            body: {
                cell: EditableCell,
            },
        };

        const columns = this.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: col.dataIndex === 'age' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <EditableContext.Provider value={this.props.form}>
                <Table
                    components={components}
                    bordered
                    dataSource={this.state.data}
                    columns={columns}
                    rowClassName="editable-row"
                    pagination={{
                        onChange: this.cancel,
                    }}
                />
            </EditableContext.Provider>
        );
    }
}

export default EditableTable


