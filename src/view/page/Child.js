import React from 'react'

export default class Child extends React.Component{
    constructor(props) {
        super(props)

        this.state = {
            show: true
        }
    }

    static getDerivedStateFromProps(nextProps,prevState) {
        console.log('getDerivedStateFromProps','-',nextProps,'-',prevState);
        const ps = prevState.value
        if(ps){
            return {
                ps:'ppp'
            }
        }
        return {
            data:'哈哈哈'
        }
    }

    componentDidMount(){
        console.log('componentDidMount');
    }

    shouldComponentUpdate(nextProps,nextState){
        console.log('shouldComponentUpdate','-',nextProps,'-',nextState);
        return true
    }

    componentDidUpdate(prevProps,prevState,info){

        console.log('componentDidUpdate','-',prevProps,'-',prevState,'-',info);
    }

    getSnapshotBeforeUpdate(){
        console.log('getSnapshotBeforeUpdate');
        return 'hello dog'
    }

    componentWillUnmount(){
        console.log('componentWillUnmount');
    }

    renderState = ()=>{
        this.setState({
            value:'vvv'
        })
    }

    render() {
        console.log('render',this.state);
        return (
            <div>
                demo
                <br />
                <button onClick={()=>{this.renderState()}}>setState</button>
            </div>
        )
    }


}