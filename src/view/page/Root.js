/*
 * @Autor: wangchao
 * @Date: 2019-07-23 14:47:15
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-29 09:48:58
 * @Description: file content
 */
import React from 'react';
import FormatUtil from '../../utils/FormatUtil';
import { HigherComponent } from '../../base/HigherComponent';
const wx = require('weixin-js-sdk');
import shareLink from '../../utils/WxShareTool';
import APIService, { YH_HttpResponseStatus, Status } from '../../service/APIService';
import TabBars2 from '../component/TabBars2';
import Container from '../commonComponent/Container';
import { Prompt } from 'react-router-dom';

import { Button, Select as SelectAnt, Modal, Form, Input } from 'antd';
// import 'react-virtualized/styles.css'
import Select22 from 'react-select-virtualized';

const Option = SelectAnt.Option;


import cssObj from '@/style/index.less';

@Form.create()
@HigherComponent()
class Root extends React.Component {
    constructor(props) {
        super(props)


    }

    //关闭或者刷新
    listener = (e) => {
        e.preventDefault();
        e.returnValue = '离开当前页后，所编辑的数据将不可恢复'
    }


    componentDidMount() {
        FormatUtil.setTitle('root页面');

        this.props.dispatch((dispatch) => {
            setTimeout(() => {
                dispatch({ type: '0327' })
            }, 3000)
        })
        this.wxShareConfig()

        window.addEventListener('beforeunload', this.listener)
    }


    wxShareConfig = () => {
        //微信分享配置
        wx.config({
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: '', // 必填，公众号的唯一标识
            timestamp: '', // 必填，生成签名的时间戳
            nonceStr: '', // 必填，生成签名的随机串
            signature: '',// 必填，签名
            jsApiList: ['updateAppMessageShareData', 'updateTimelineShareData'] // 必填，需要使用的JS接口列表
        });
        this.forShare()
    }

    forShare = () => {
        let linkUrl = location.origin + location.pathname + location.hash;
        let imgUrl = '';
        let title = '';
        let desc = '';

        shareLink(title, desc, linkUrl, imgUrl, this.successCallback)
    }

    successCallback = () => {
        console.log('success!!!')
    }

    onChange = (p) => {
        console.log('111111', p)
    }



    render() {

        let list = []
        for (let i = 0; i < 50; i++) {
            list.push({
                label: `name${i}`,
                value: i,
            })
        }

        const { getFieldDecorator } = this.props.form

        return (
            <Container style={{ ...undefined }}>
                <div style={{ overflow: 'auto', flex: 1 }}>
                    <p>Root~~~</p>
                    <div className={cssObj.title}>
                        <div className='col'>哈哈哈</div>
                    </div>
                    <div>
                        ********************************************************
                    </div>
                    <div>
                        {
                            getFieldDecorator('select', {
                                rules: [{
                                    required: true,
                                    message: '嘿嘿嘿'
                                }],
                                initialValue: { key: '6688', label: 'ok' }
                            })(
                                <SelectAnt
                                    style={{ width: 300 }}
                                    showSearch
                                    labelInValue
                                    optionFilterProp='children'
                                    filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {
                                        list.map((item, i) => {
                                            return (
                                                <Option key={item.value}>{item.label}</Option>
                                            )
                                        })
                                    }
                                </SelectAnt>
                            )
                        }

                    </div>
                    <div>
                        ***********************************************************
                    </div>
                    <Form className='wcc'>
                        <Form.Item >
                            <div style={{ marginTop: '10px' }}>
                                {
                                    getFieldDecorator('vSelect', {
                                        rules: [{
                                            required: true,
                                            message: '哈哈哈'
                                        }],
                                        initialValue: { label: 'name99' }
                                    })(
                                        <Select22 options={list} />
                                    )
                                }
                            </div>
                        </Form.Item>
                    </Form>

                    <div>
                        ***********************************************************
                    </div>
                    <Button type='primary' onClick={() => this.props.history.push('/About')}> 跳转</Button>
                    <Button type='primary' onClick={() => this.confirm()}>确定</Button>
                </div>
                <TabBars2></TabBars2>
                <Prompt message={(location, action) => {
                    sessionStorage.setItem('location', JSON.stringify(this.props.history.location))
                    console.log('123', action)
                    return action
                }} when={true} />
            </Container>
        )
    }

    confirm = () => {
        const { validateFields } = this.props.form
        validateFields((err, values) => {
            if (!err) {
                console.log('vvvvvv', values)
            }
        })

    }
}

export default Root
