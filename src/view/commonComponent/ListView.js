/*
 * @Autor: wangchao
 * @Date: 2019-09-06 17:26:16
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-20 15:29:15
 * @Description: 列表展示
 */
import React, { Component } from 'react';
import { ActivityIndicator } from 'antd-mobile';

const pageSize = 10;

export default class ListView extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }

        this.curPage = 0;
        this.isRefresh = false;
        this.isLoading = true;
    }

    //下拉刷新
    onRefresh = () => {
        const { onRefresh } = this.props;
        if (onRefresh) {
            console.log('下拉刷新')
            this.isRefresh = true;
            this.isLoading = true;
            this.curPage = 0;
            onRefresh(this.curPage)
        }
    }

    //上拉加载更多
    onEndReached = () => {
        const { onEndReached } = this.props;
        if (!this.isLoading && (this.curPage * pageSize <= data.length) && onEndReached) {
            console.log('加载更多')
            this.isLoading = true;
            this.curPage += 1
            onEndReached(this.curPage)
        }
    }

    renderItem = (item, index) => {
        const { renderItem } = this.props;
        return renderItem(item, index)
    }

    render() {
        const { isLoading } = this.props

        if (isLoading) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                    <ActivityIndicator animating={true} />
                </div>
            )
        }

        this.isLoading = false;
        this.isRefresh = false;
        const {
            dataSource,
            renderHeader,
            renderFooter,
            renderEmpty,
        } = this.props

        if (!dataSource || dataSource.length == 0) {
            return (
                renderEmpty ? renderEmpty() : null
            )
        }
        return (
            <div className='listView' style={{ display: 'flex', flexDirection: 'column', overflow: 'auto',flex:1 }}>
                {renderHeader ? renderHeader() : null}
                {
                    dataSource.map((item, index) => {
                        return (
                            <div key={`item${index}`}>
                                {this.renderItem(item, index)}
                            </div>
                        )
                    })
                }
                {renderFooter ? renderFooter() : null}
            </div>
        )
    }
}
