/*
 * @Autor: wangchao
 * @Date: 2019-09-03 17:46:39
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-04 09:37:50
 * @Description: 数据为空时显示组件
 */
import React, { Component } from 'react'

export default class EmptyView extends Component {
    render() {
        const { title, imgUrl, style, imageStyle } = this.props
        return (
            <div style={{ ...style, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
                <img style={imageStyle} src={imgUrl}></img>
                <span style={{ fontSize: '14px', color: '#999' }}>{title}</span>
            </div>
        )
    }
}
