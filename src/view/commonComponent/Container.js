/*
 * @Autor: wangchao
 * @Date: 2019-09-03 17:35:57
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-03 17:46:10
 * @Description: 容器组件
 */
import React from 'react'

const Container = (props) => {
    return (
        <div style={{display:'flex',flexDirection:'column',height:'100vh',overflow:'auto',wordBreak:'break-word'}}>
            {props.children}
        </div>
    )
}

export default Container
