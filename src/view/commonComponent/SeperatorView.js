/*
 * @Autor: wangchao
 * @Date: 2019-09-04 09:58:32
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-04 10:26:23
 * @Description: 分割线
 */

import React, { Component } from 'react'

export default class SeperatorView extends Component {
    render() {
        const { style } = this.props
        return (
            <div style={{width:'100%',height:'1',backgroundColor:'#f4f4f4',...style}}></div>
        )
    }
}

