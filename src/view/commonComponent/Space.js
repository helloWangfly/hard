/*
 * @Autor: wangchao
 * @Date: 2019-09-04 10:26:41
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-06 09:16:34
 * @Description: 空格组件
 */
import React, { Component } from 'react'

export default class Space extends Component {
    render() {
        const { height, groundColor } = this.props
        return (
            <div style={{height:height,backgroundColor:groundColor}}></div>
        )
    }
}
