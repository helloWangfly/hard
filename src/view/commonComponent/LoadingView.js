/*
 * @Autor: wangchao
 * @Date: 2019-09-04 09:38:25
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-04 09:48:47
 * @Description: 加载中组件
 */
import React, { Component } from 'react'
import { ActivityIndicator } from 'antd-mobile'

export default class LoadingView extends Component {
    render() {
        const { title, image, style } = this.props
        return (
            <div style={{ ...style, display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '300px' }}>
                <ActivityIndicator animating={true} />
                <span style={{fontSize:'14',color:'#666',marginTop:'20'}}>努力加载中...</span>
            </div>
        )
    }
}
