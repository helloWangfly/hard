/*
 * @Autor: wangchao
 * @Date: 2019-09-20 09:11:50
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-11 17:21:23
 * @Description: file content
 */
import React from 'react';
import FormatUtil from '../../utils/FormatUtil';
import { HigherComponent } from '../../base/HigherComponent';
import { LinkTo } from '../../utils/LinkTo';
import { TabBar } from 'antd-mobile';

@HigherComponent()
export default
    class TabBars2 extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedTab: '/Root'
        }

        this.routerTabBar = [
            {
                path: '/Root',
                name: '首页',
                icon: require('../../images/home1.png'),
                selectedIcon: require('../../images/home2.png')
            },
            {
                path: '/About',
                name: 'about',
                icon: require('../../images/about1.png'),
                selectedIcon: require('../../images/about2.png')
            },
            {
                path: '/Vip',
                name: 'vip',
                icon: require('../../images/vip1.png'),
                selectedIcon: require('../../images/vip2.png')
            },
            {
                path: '/Hook',
                name: 'hook',
                icon: require('../../images/vip1.png'),
                selectedIcon: require('../../images/vip2.png')
            }
        ]
    }

    componentWillMount() {
        if (this.props.selectedTab) {
            this.setState({
                selectedTab: this.props.selectedTab
            })
        }
    }

    render() {
        return (
            <div>
                <TabBar
                    unselectedTintColor='#666'
                    tintColor='#ff8316'
                    barTintColor='#fff'
                >
                    {
                        this.routerTabBar.map((router, index) => (
                            <TabBar.Item
                                key={router.path}
                                icon={{ uri: router.icon }}
                                selectedIcon={{ uri: router.selectedIcon }}
                                title={router.name}
                                selected={this.state.selectedTab === router.path}
                                onPress={() => {
                                    LinkTo.push({ pathname: router.path })
                                }}
                            >
                            </TabBar.Item>
                        ))

                    }
                </TabBar>
            </div>
        )
    }
}