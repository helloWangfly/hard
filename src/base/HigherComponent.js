/*
 * @Autor: wangchao
 * @Date: 2019-07-31 15:55:08
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-19 14:21:21
 * @Description: 高阶组件
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Toast } from 'antd-mobile';
import { Status } from '../service/APIService'

export const HigherComponent = (param) => (WrapperComponent) => {
    class MiddleComponent extends Component {
        constructor(props) {
            super(props);
            this.state = {

            }
        }

        shouldComponentUpdate(nextProps, nextState) {
            const { status, data } = nextProps
            if (status == Status.ERROR) {
                const msg = data.message ? data.message : '请求失败'
                Toast.info(msg, 2)
                return true
            }

            return true;
        }


        render() {
            return (
                <WrapperComponent {...this.props} />

            )
        }
    }

    const mapStateToProps = (store) => {
        return {
            type: store.common.type,
            status: store.common.status,
            data: store.common.data,
        }
    }

    return connect(mapStateToProps)(MiddleComponent)
}
