/*
 * @Autor: wangchao
 * @Date: 2019-07-07 18:37:20
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-15 16:50:01
 * @Description: file content
 */

import React from 'react';
import ReactDom from 'react-dom';
import App from './forApp'

//redux
// import { createStore } from 'redux';
// import reducer from './reducers';
import { Provider } from 'react-redux';
import store from './store/DataStore';

// const store = createStore(reducer);

function render(type) {
    console.log('type==', type)
    ReactDom.render(<Provider store={store}><App /></Provider>, document.getElementById('app'))
}

render('out')

if (module.hot) {
    module.hot.accept('./forApp.js', () => {
        render('in')
    })
}