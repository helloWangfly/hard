/*
 * @Autor: wangchao
 * @Date: 2019-09-03 09:13:54
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-03 14:44:05
 * @Description: file content
 */
export const LinkTo = {
    replace: (_url) => {
        let url = LinkTo.urlHandle(_url)
        location.replace(url)
    },
    push: (_url) => {
        let url = LinkTo.urlHandle(_url)
        location.hash = url
    },
    getParams: (url) => {
        let urlArr = []
        let query = {}
        url = decodeURIComponent(url.slice(1))
        urlArr = url.split('&')
        urlArr.forEach(elem => {
            if (elem && elem != '') {
                let key = elem.split('=')[0]
                let value = decodeURIComponent(elem.split('=')[1])
                try {
                    value = JSON.parse(value)
                    query[key] = value
                } catch{
                    query[key] = value
                }
            }
        });
        return query
    },
    urlHandle: (url) => {
        let finalUrl = ''
        if (url.pathname) {
            let urlQuery = url.query;
            //不存在或者空对象
            if (!urlQuery || Object.keys(urlQuery).length == 0) {
                finalUrl = '#' + url.pathname
            } else {
                for (let key in urlQuery) {
                    let value = urlQuery[key]
                    if (value && typeof value == 'object' && Object.keys(value).length > 0) {
                        value = JSON.stringify(value)
                    }
                    if (value) {
                        finalUrl = finalUrl + '&' + key + '=' + encodeURIComponent(value)
                    }
                }
                finalUrl = '#' + url.pathname + finalUrl.replace(/^[&]/, '?')
            }
        }
        return finalUrl
    }
}