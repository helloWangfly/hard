export default class FormatUtil {
    // 设置标题
    static setTitle(text) {
        document.title = text;

    }

    //sessionStorage获取与设置
    static setSessionStorage(name, value) {
        const str = JSON.stringify(value);
        sessionStorage.setItem(name, str);
    }

    static getSessionStorage(name){
        const str = sessionStorage.getItem(name);
        const obj = JSON.parse(str);
        return obj;
    }

    //localStorage获取与设置
    static setLocalStorage(name,value){
        const str = JSON.stringify(value);
        localStorage.setItem(name,str);
    }

    static getLocalStorage(name){
        const str = localStorage.getItem(name);
        const obj = JSON.parse(str);
        return obj;
    }





}