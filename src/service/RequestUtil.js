/*
 * @Autor: wangchao
 * @Date: 2019-08-12 09:56:48
 * @LastEditors: wangchao
 * @LastEditTime: 2019-08-13 13:35:17
 * @Description: 生成request
 */

const serviceURLConfig = {
    RELATIVE: '',
    DEV_URL: 'https://hims-mobile-stg1.pingan.com.cn'  //测试域名
}

export const serverUrl = BUILD_ENV == 'development' ? serviceURLConfig.DEV_URL : serviceURLConfig.RELATIVE

export default class RequestUtil {


    static seriesRequest(method, path, params, headers) {
        let url = `${serverUrl}${path}`

        if (headers == undefined) {
            headers = {}
            headers["Content-Type"] = "application/json;charset=utf-8"
        }

        let userToken = ''
        headers["X-Authorization"] = "Bearer " + userToken
        headers["X-Requested-with"] = "XMLHttpRequest"

        if (params == undefined) {
            params = {}
        }

        let request;
        if (method == 'GET') {
            let paramsArray = [];
            Object.keys(params).forEach(key => paramsArray.push(key + '=' + encodeURIComponent(params[key])))
            if (paramsArray.length > 0) {
                url += '?' + paramsArray.join('&')
            }
            request = new Request(url, {
                method: 'GET',
                headers: headers
            })
        } else if (method == 'POST') {
            let bodyStr = JSON.stringify(params);
            request = new Request(url, {
                method: 'POST',
                headers: headers,
                body: bodyStr
            })
        } else if (method == 'PUT') {
            request = new Request(url, {
                method: 'PUT',
                headers: headers,
                body: JSON.stringify(params)
            })
        } else if (method == 'DELETE') {
            request = new Request(url, {
                method: 'DELETE',
                headers: headers,
                body: JSON.stringify(params)
            })
        }
        console.log('#######################发出的请求', request.url)
        return request;
    }
}
