/*
 * @Autor: wangchao
 * @Date: 2019-08-12 09:55:33
 * @LastEditors: wangchao
 * @LastEditTime: 2019-08-12 15:28:17
 * @Description: fetch API请求
 */

import RequestUtil from './RequestUtil'
import RequestDispatch from './RequestDispatch'

//HTTP请求结果
export const YH_HttpResponseStatus = {
    Success: 1,//成功
    Failure: 0 //失败
}

//HTTP请求状态
export const Status = {
    INIT: 'INIT',
    LOADING: 'LOADING',
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR'
}

//api接口
export default class APIService {

    //
    static getInfo1() {
        let request = RequestUtil.seriesRequest('GET', `/aa/bb/cc`, obj)
        return RequestDispatch.handleRequest(request)
    }

    //
    static getInfo2() {
        let request = RequestUtil.seriesRequest('POST', `/aa/bb/cc`, obj)
        return RequestDispatch.handleRequest(request)
    }

    //
    static getInfo3() {
        let request = RequestUtil.seriesRequest('PUT', `/aa/bb/cc`)
        return RequestDispatch.handleRequest(request)
    }

    //
    static getInfo4() {
        let request = RequestUtil.seriesRequest('DELETE', `/aa/bb/cc`)
        return RequestDispatch.handleRequest(request)
    }




}
