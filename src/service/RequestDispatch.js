/*
 * @Autor: wangchao
 * @Date: 2019-08-13 13:23:53
 * @LastEditors: wangchao
 * @LastEditTime: 2019-09-19 11:35:15
 * @Description: 网络请求层
 */

import { ActionTypes } from '../store/constants';
import { Status, YH_HttpResponseStatus } from './APIService';
import store from '../store/DataStore';
import 'whatwg-fetch';

export default class RequestDispatch {
    static dispatchLoadingStatus(status, data) {
        store.dispatch({
            type: ActionTypes.BASE,
            status: status,
            data: data
        })
    }


    /**
     * @author: wangchao
     * @param {type} 
     * @return: 
     * @description:接口响应处理
     */

    static handleRequest(request) {
        this.dispatchLoadingStatus(Status.LOADING, {})
        return fetch(request).then(res => {
            console.log('#####resresres',res)
            if (res.status >= 200 && res.status < 300) { //此处判断的是http返回的状态码
                return res
            } else {
                throw res
            }
        }).then(async response => {
            const json = await response.json()
            console.log('###################请求结果', json)
            if (json.status && json.status != 200) {//此处的200是业务服务器返回的状态码
                throw json
            } else {
                this.dispatchLoadingStatus(Status.SUCCESS, json)
                return { ...json, status: YH_HttpResponseStatus.Success }
            }
        }).catch(async err => {
            console.log('err',err)
            try {
                const result = await err.json()
                this.dispatchLoadingStatus(Status.ERROR, result)
                return { ...result, status: YH_HttpResponseStatus.Failure }
            } catch{
                this.dispatchLoadingStatus(Status.ERROR, err)
                return { status: YH_HttpResponseStatus.Failure }
            }
        })
    }



}
