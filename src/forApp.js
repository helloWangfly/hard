/*
 * @Autor: wangchao
 * @Date: 2019-07-07 18:37:20
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-15 16:50:01
 * @Description: file content
 */

import React from 'react';
import ReactDom from 'react-dom';
import UrlConfig from './service/UrlConfig';
import { BrowserRouter, HashRouter, Route, NavLink, Switch, Redirect } from 'react-router-dom';
import { routerMaps } from './router/router';
import Loadable from 'react-loadable';
import Loading from './view/commonComponent/LoadingView';



// import 'antd/dist/antd.css';
import 'antd-mobile/dist/antd-mobile.css';
import { Button, Toast, ActivityIndicator, Modal } from 'antd-mobile';
import { Spin } from 'antd';
const alert = Modal.alert;

import Root from './view/page/Root';
import About from './view/page/About';
import Vip from './view/page/Vip';

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {


  }


  // 动态获取路由
  dynamicRouter = () => {
    const dynamicImport = (component) => Loadable({
      loader: () => import('./view/page' + component + '.js'),
      loading: Loading
    })

    const defaultRoute = [
      <Route exact path="/" key="index" render={() => (<Redirect exact to={routerMaps[0].path} />)} />
    ]

    return defaultRoute.concat(routerMaps.map((route, index) => {
      return (<Route exact key={index} path={route.path} component={dynamicImport(route.component)} />)
    }))
  }

  getConfirmation = (action, callback) => {
    alert('', '欣赏一下妾身的舞姿吧？', [
      {
        text: '留下来', onPress: () => {
          if (action === 'POP') {
            const locationObj = JSON.parse(sessionStorage.getItem('location'))
            const url = `${locationObj.pathname}${locationObj.search}`
            window.location.hash = url
            return
          }
          callback(false)
        }
      },
      { text: '那我走', onPress: () => callback(true) },
    ])

  }

  //getUserConfirmation={this.getConfirmation}

  render() {
    return (
      <HashRouter getUserConfirmation={this.getConfirmation}>
        <div>
          <div>
            <ul>
              <li><NavLink exact to="/" activeStyle={{ color: 'green' }}>首页</NavLink></li>
              <li><NavLink exact to="/About" activeStyle={{ color: 'green' }}>About</NavLink></li>
              <li><NavLink exact to="/Vip" activeStyle={{ color: 'green' }}>Vip</NavLink></li>
            </ul>
          </div>
          <Switch>
            {this.dynamicRouter()}
          </Switch>
        </div>
      </HashRouter>
    )
  }

}

export default App