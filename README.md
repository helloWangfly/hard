## 尝试在手机上 去进行项目的预览和测试
  1. 要保证自己的手机可以正常运行
  2. 要保证 手机 和 开发项目的电脑 处于同一个 WIFI 环境中，也就是说 手机 可以 访问到 电脑的 IP
  3. 打开自己的项目中 package.json 文件，在 start 脚本中，添加一个 --host 指令，把 当前 电脑的 WIFI IP地址，设置为 --host 的指令值；
    + 如何查看自己电脑所处 WIFI 的IP呢，在 cmd 终端中运行 `ipconfig` , 查看 无线网的 ip 地址
    如：--host 192.168.43.228

## 如何启用jsx语法？
  1. 安装 babel 插件
    * 运行 npm i babel-core babel-loader babel-plugin-transform-runtime -D
    * 运行 npm i babel-preset-env babel-preset-stage-0 -D
  2. 安装能够识别转换jsx语法的包 babel-preset-react 
    * 运行 npm i babel-preset-resct -D
  3. 添加 .babelrc 配置文件
     {
       "presets":["env","stage-0","react"],
       "plugins":["transform-runtime"]
     }

   备注：第3步 也可以在webpack.config.js 中配置 options 选项

## js引用数据类型拷贝方法
  譬如:  let obj1 = {name:'cc',o:{age:'20'}}
         let arr1 = {'ww','cc',{name:'wc',[11,22]}}
  1. 浅拷贝
    （1）概念：拷贝对象第一层属性，属性值是引用类型的数据引用地址不改变
    （2）方法：
      对象浅拷贝方法：
       第一种：let obj2 = {}  for(let key in obj1){obj2[key] = obj1[key]}  
       第二种：let obj3 = Object.assign({},obj1)
       第三种：let obj4 = {...obj1}
      数组浅拷贝方法：
       第一种：let arr2 = arr1.concat([])
       第二种：let arr3 = arr1.slice(0)
       第三种：let arr4 = [...arr1]
       第四种：let arr5 = Array.from(arr1)
       第五种：let arr6 = []  for(let i = 0;i < arr1.length; i++){arr6[i] = arr1[i]}
      
  2. 深拷贝
    （1）概念：拷贝对象各个层级的属性，复制出来的每个对象都有属于自己的内存空间
    （2）方法：
       第一种：let result = JSON.parse(JSON.stringify(scourse))  <!--（缺点：函数丢失）-->
       第二种：递归 
            function deepCopy(scourse){
                let target;
                if(typeof scourse !== 'object'){
                  return target = scourse
                }

                target = Array.isArray(scourse) ? [] : {}

                for(let key in scourse){
                    if(typeof scourse[key] == 'object'){
                      target[key] = deepCopy(scourse[key])
                    }else{
                      target[key] = scourse[key]
                    }
                }

                return target
            }
      <!-- 疑问：为什么无论深浅拷贝，都把函数当成基本数据类型看待？ -->

## GitLab账号密码
  （1）登录
      账号：1871365900@qq.com
      密码：wc123456
  （2）拉取代码
      账号：helloWangfly
      密码：wc123456
             
## 静态资源开启一个服务
  1. npx serve public(指定文件目录,默认当前目录)
  2. npx http-server dist(指定文件目录,默认当前目录./public)
  3. express.static(__dirname + '/public')

## webpack打包命令
  1. 终端命令 webpack 会使用全局安装的webpack
  2. npm run aa(package.json中scripts配置的脚本，会使用当前项目安装的webpack，若没有会一层一层往上查找node_modules目录)
  3. npx webpack 
    
## npm i webpack-dev-server -D，不在json脚本里面配置，启动本地服务命令：npx webpack serve

