/*
 * @Autor: wangchao
 * @Date: 2019-07-15 09:36:11
 * @LastEditors: wangchao
 * @LastEditTime: 2019-10-15 17:16:32
 * @Description: file content
 */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin'); 
const WebpackBar = require('webpackbar');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const rules = {
    rules: [
        {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', { 'useBuiltIns': 'usage' }],//{'debug':true}
                        ["@babel/preset-react"]
                    ],
                    plugins: [
                        ['@babel/plugin-proposal-decorators', { "legacy": true }],
                        [
                            "@babel/plugin-transform-runtime",
                            {
                                "absoluteRuntime": false,
                                "corejs": 2,
                                "helpers": true,
                                "regenerator": true,
                                "useESModules": false
                            }
                        ],
                        ["@babel/plugin-proposal-class-properties", { "loose": true }],
                        ["import", { "libraryName": "antd-mobile", "style": 'css' },'antd-mobile'],
                        ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": 'css' },'antd'] // `style: true` 会加载 less 文件
                    ]
                }
            }
        },
        {
            test: /\.css$/i,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options:{
                        publicPath:'../'
                    }
                },
                'css-loader',
            ],
        },
        {
            test: /\.(scss|sass)$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options:{
                        publicPath:'../'
                    }
                },
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        },
        {
            test: /\.less$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options:{
                        publicPath:'../'
                    }
                },
                'css-loader', // translates CSS into CommonJS
                'less-loader', // compiles Less to CSS
            ]
        },
        {
            test: /\.(png|jpg|gif|svg)$/,
            use: [
              {
                loader: 'url-loader',
                options: {
                    limit:4*1024,
                    name: '[name].[ext]',
                    outputPath: 'images/'
                },
              },
              {
                loader: 'image-webpack-loader'
              }
            ],
        },
    ]
}

const entry = {
    entry: {
        app: './src/App.js',
        hello: './src/hello.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: '首页',
            filename: 'index.html',
            template: 'src/public/index.html',
            hash: true,
            favicon:'./src/logo.ico',
            chunks: ['app']
        }),
        new HtmlWebpackPlugin({
            title: 'hello',
            filename: 'hello.html',
            template: 'src/public/index.html',
            hash: true,
            favicon:'./src/logo.ico',
            chunks: ['hello']
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'css/[name].[hash].css',
            chunkFilename: 'css/[id].[hash].css',
        }),
        new CleanWebpackPlugin(),
        new webpack.ProvidePlugin({
            $:"jquery",
            jQuery:"jquery",
            "window.jQuery":"jquery"
        }),
        new WebpackBar(),
        new webpack.DefinePlugin({
            BUILD_ENV: JSON.stringify('production')
        })
    ],
    optimization: { //优化项
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap:true
            }),
            new OptimizeCSSAssetsPlugin({})
        ],
        splitChunks:{ //分割代码块
            cacheGroups:{ //缓存组
                default:{ //公共的模块
                    chunks:'initial', //初始化开始抽取
                    minSize:0, //所有文件
                    minChunks:2 //引用次数
                },
                vendor:{ //第三方模块
                    priority:1, //权重
                    test:/node_modules/,
                    chunks:'initial',
                    minSize:0,
                    minChunks:2
                }
            },
            chunks: 'all'
        }
        
    },
}

const output = {
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].[hash].bundle.js",
        // chunkFilename:'./js/[name].[chunkhash].chunk.js'
    },
    module: rules,
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 8082
    },
    mode:'production',
    resolve:{
        extensions:['.js','.jxs','.json'], // 表示，这几个文件的后缀名，可以省略不写
        alias:{ //表示别名
            '@':path.join(__dirname,'./src') // 这样，@ 就表示项目根目录中 src 的这一层路径
        }
    },
    // watch:true, //自动打包
    // watchOptions:{
    //     poll:1000,
    //     aggregateTimeout:500,
    //     ignored:/node_modules/
    // }
}

Object.assign(entry,output)

module.exports = entry;

// module.exports = (env,argv)=>{
//     entry.plugins = entry.plugins.concat([
//         new webpack.DefinePlugin({
//             BUILD_ENV : JSON.stringify(argv.mode)
//         })
//     ])
//     return entry
// }